$(document).ready(function() {

	/**
	 * Import JS files
	 */
	$('head').append('<link rel="stylesheet" href="/static/css/bootstrap/bootstrap-datetimepicker.min.css">');

	var jsfiles = ['js/jquery.maskedinput.min.js', 
				   'js/moment.min.js',
				   'js/bootstrap/bootstrap-datetimepicker.min.js',
				   'js/bootstrap/locales/bootstrap-datetimepicker.pt-BR.js']
			
	$.each(jsfiles, function(i, js){
		$('body').append('<script src="/static/'+js+'"></script>');
	});
	
	/**
	 * Definicao dos icones
	 */
	var icons = {time: "fa fa-clock-o",
				 date: "fa fa-calendar",
				 up: "fa fa-arrow-up",
				 down: "fa fa-arrow-down"
		}

	/**
	 * Add HTML do icone
	 */
	$('.datetimePicker, .datePicker, .timePicker').append('<span class="input-group-addon"><span></span></span>');
	

	/**
	 * Date Time
	 */
	$('.datetimePicker input').mask("99/99/9999 - 99:99");
	$('.datetimePicker').attr('data-date-format', 'DD/MM/YYYY - HH:mm');
	$('.datetimePicker').datetimepicker({
		icons: icons,
		language: 'pt-BR',
		useSeconds: false,
	});

	/**
	 * Date
	 */
	$('.datePicker input').mask("99/99/9999");
	$('.datePicker').attr('data-date-format', 'DD/MM/YYYY');
	$('.datePicker').datetimepicker({
		icons: icons,
		language: 'pt-BR',
		pickTime: false,
	});
	
	/**
	 * Time
	 */
	$('.timePicker input').mask("99:99");
	$('.timePicker').attr('data-date-format', 'HH:mm');
	$('.timePicker').datetimepicker({
		icons: icons,
		language: 'pt-BR',
		pickDate: false,
		useSeconds: false,
	});
});