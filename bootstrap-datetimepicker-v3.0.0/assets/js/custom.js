$(document).ready(function() {
	/**
	* Popover bootstrap
	**/
    $('[data-toggle="popover"]').popover({html : true});

	/**
	* Adiciona Class do Bootstrap nos selects
	**/
	$('select').addClass('form-control');

	/**
	* Redimensiona textarea de acordo com o conteudo
	**/
	$('textarea').each(function(){ 
		val = $(this)[0].scrollHeight;
		if (val > 0){
			$(this).height(val);
		}
	});
	
	//$('[data-toggle="popover"]').popover({html : true});
	
});

/**
 * Modal
 */

$(document).on('click', '.openModal', function(){
    $.ajax({
        type: 'GET',
        url: $(this).attr('rel'),
        dataType: 'html',
        success: function(retorno){
            $('#modal').html(retorno);
        },
        cache: false
    });
});

/**
* Adiciona pontos e virgulas em valores monetarios (R$)
**/
function addCommas(str) {
	str = parseFloat(str).toFixed(2).toString();
	
    var parts = (str + "").split("."),
        main = parts[0],
        len = main.length,
        output = "",
        i = len - 1;

    while(i >= 0) {
        output = main.charAt(i) + output;
        if ((len - i) % 3 === 0 && i > 0) {
            output = "." + output;
        }
        --i;
    }
    if (parts.length > 1) {
        output += "," + parts[1];
    }
    return output;
}

/* Abrevia números */
function abrevNum(num) {
     if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'Bi';
     }
     if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'Mi';
     }
     if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + ' mil';
     }
     return parseInt(num);
}

// Converte os valores dos campos de reais e percetuais para float
function get_float(filter, tipo){
    if ($(filter).val()){
        if (tipo == 'reais'){
            return parseFloat($(filter).val().split('R$ ')[1].replace('.','').replace('.','').replace('.','').replace(',','.'));
        } else {
            return parseFloat($(filter).val().split('%')[0].replace('.','').replace('.','').replace('.','').replace(',','.'))
        }
    } else {
        return 0.0
    }
}

function defaultMaskMoney(){
    $('.valorReal').maskMoney({ prefix: '', thousands: '.', decimal: ',' });
    $('.valorQtd').maskMoney({ prefix: '', thousands: '', decimal: '.' });
    
};

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;
    if((tecla>47 && tecla<58)) 
        return true;
    else {
        if (tecla==8 || tecla==0) 
            return true;
        else
            return false;
    }
};

function defaultMaskFone(){
    $(".telefone").mask("(99) 9999-9999?9");
    $(".telefone").focusout(function (event) {  
        var target, phone, element;  
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
        phone = target.value.replace(/\D/g, '');
        element = $(target);  
        element.unmask();  
        if(phone.length > 10) {  
            element.mask("(99) 99999-999?9");  
        } else {  
            element.mask("(99) 9999-9999?9");  
        }  
    });
}
        
function defaultMaskDate(){
    $(document).on('focus', '.campoData', function(){
        $(this).mask("99/99/9999");
    });
    $(document).on('focus', '.campoHora', function(){
        $(this).mask("99:99");
    });
    // validação simples do preenchimento da hora
    $('.timePicker, .campoHora').on('change', function(){
        var input = $(this);  
        if (input.hasClass('timePicker')) var input = $(this).children();  
        var time = input.val();
        var hora = time.split(':')[0]
        var minuto = time.split(':')[1]
        
        if (hora > 23 || minuto > 60){
            alert("Horário inválido");
            input.val("").focus();
        }
    });
}

function defaultMask(){
    defaultMaskMoney();
    defaultMaskFone();
    defaultMaskDate()
};