$(document).ready(function() {
	/**
	 * Import JS file
	 */
	$('body').append('<script src="/static/js/bootbox.min.js"></script>');
	
    /** Botão com confirmação **/
    $('.getConfirmation').click(function(e) {
        bnt = $(this);
        e.preventDefault();
        bootbox.confirm({
            message: bnt.attr('msg') || 'Tem certeza que dejesa continuar?',
            buttons: {
                confirm: { label: 'Sim', className: 'btn-success' },
                cancel: { label: 'Não', className: 'btn-danger' }
            },
            callback: function (result) {
                if (result) {
                    $('form').append('<input type="hidden" name="'+bnt.attr('name')+'"/>').submit();
                }
            }
        });
    });
});